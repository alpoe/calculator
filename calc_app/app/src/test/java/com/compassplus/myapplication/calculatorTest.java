package com.compassplus.myapplication;

import org.junit.Assert;
import org.junit.Test;

import java.security.spec.ECField;

public class calculatorTest {
    @Test
    public void clearOperation_setUndefinedOperation() {
        calculator clc = new calculator();
        clc.setAddition();

        clc.clearOperation();

        Assert.assertFalse(clc.isOperationDefined());
    }

    @Test
    public void isOperationDefined_operationDefined() {
        calculator clc = new calculator();
        clc.setAddition();

        Assert.assertTrue(clc.isOperationDefined());
    }

    @Test(expected = IllegalArgumentException.class)

    public void illegalArg() throws Exception{

        calculator clc = new calculator();
        clc.setOperands(5, 0);
        clc.setDivision();
        clc.doOperation();
    }

    @Test(expected = ArithmeticException.class)

    public void arithExc() throws Exception{
        calculator clc = new calculator();
        clc.setOperands(5, 5);
        clc.doOperation();
    }

    @Test
    public void operationIsAllowed() throws Exception {
        calculator clc = new calculator();
        clc.setOperands(5, 5);
        clc.setDivision();
        Assert.assertTrue(clc.isOperationAllowed());
    }
    @Test
    public void operationIsNotAllowed() throws Exception {
        calculator clc = new calculator();
        clc.setOperands(5, 0);
        clc.setDivision();
        Assert.assertFalse(clc.isOperationAllowed());
    }

    @Test
    public void additionIsCorrect() throws Exception{
        calculator clc = new calculator();
        clc.setOperands(5, 25);
        clc.setAddition();
        Assert.assertEquals(30, clc.doOperation(), 0);
    }

    @Test
    public void subtractionIsCorrect() throws Exception{
        calculator clc = new calculator();
        clc.setOperands(5, 25);
        clc.setSubtraction();
        Assert.assertEquals(-20, clc.doOperation(), 0);
    }

    @Test
    public void multiplicationIsCorrect() throws Exception{
        calculator clc = new calculator();
        clc.setOperands(5, 25);
        clc.setMultiplication();
        Assert.assertEquals(125, clc.doOperation(), 0);
    }
    @Test
    public void divisionIsCorrect() throws Exception{
        calculator clc = new calculator();
        clc.setOperands(5, 25);
        clc.setDivision();
        Assert.assertEquals(0.2, clc.doOperation(), 0);
    }
}
