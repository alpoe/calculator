package com.compassplus.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends Activity {
    double arg1 = 0.0d;
    double arg2 = 0.0d;
    char operation = ' ';
    private int clickCount = 0;
    calculator clc = new calculator();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView display = findViewById(R.id.display);
        ArrayList<Button> buttonArrayList = new ArrayList<>();

        buttonArrayList.add((Button) findViewById(R.id.but0));
        buttonArrayList.add((Button) findViewById(R.id.but1));
        buttonArrayList.add((Button) findViewById(R.id.but2));
        buttonArrayList.add((Button) findViewById(R.id.but3));
        buttonArrayList.add((Button) findViewById(R.id.but4));
        buttonArrayList.add((Button) findViewById(R.id.but5));
        buttonArrayList.add((Button) findViewById(R.id.but6));
        buttonArrayList.add((Button) findViewById(R.id.but7));
        buttonArrayList.add((Button) findViewById(R.id.but8));
        buttonArrayList.add((Button) findViewById(R.id.but9));
        buttonArrayList.add((Button) findViewById(R.id.butPlus));
        buttonArrayList.add((Button) findViewById(R.id.butMinus));
        buttonArrayList.add((Button) findViewById(R.id.butMult));
        buttonArrayList.add((Button) findViewById(R.id.butDiv));
        buttonArrayList.add((Button) findViewById(R.id.butDot));
        buttonArrayList.add((Button) findViewById(R.id.butEq));
        buttonArrayList.add((Button) findViewById(R.id.delete));

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                char btnCh = ((Button) v).getText().toString().charAt(0);

                switch (btnCh) {
                    case '+':
                        arg1 = Double.parseDouble(display.getText().toString());
                        clc.setAddition();
                        display.setText("");
                        break;
                    case '-':
                        arg1 = Double.parseDouble(display.getText().toString());
                        clc.setSubtraction();
                        display.setText("");
                        break;
                    case '*':
                        arg1 = Double.parseDouble(display.getText().toString());
                        clc.setMultiplication();
                        display.setText("");
                        break;
                    case '/':
                        arg1 = Double.parseDouble(display.getText().toString());
                        clc.setDivision();
                        display.setText("");
                        break;
                    case '=':
                        arg2 = Double.parseDouble(display.getText().toString());
                        clc.setOperands(arg1, arg2);
                        display.setText("");
                        try {
                            double res = clc.doOperation();
                            String str = Double.toString(res);
                            display.setText(str);
                        } catch (IllegalArgumentException zero){
                            display.setText("Argument can't be zero");
                        }
                        catch (ArithmeticException op){
                            display.setText("Operation is not defined");
                        }
                        catch(Exception exc){
                            display.setText("some Error");
                        }
                        clickCount = 0;
                        clc.clearOperation();
                        break;
                    case 'C':
                        display.setText("");
                        clc.clearOperation();
                        break;
                    default: clickCount++;
                        if(clickCount == 1){
                            display.setText("");
                        }
                        display.setText(display.getText()+ ((Button) v).getText().toString());
                }
            }
        };

        for(Button item:buttonArrayList){
            item.setOnClickListener(listener);
        }
    }
}
