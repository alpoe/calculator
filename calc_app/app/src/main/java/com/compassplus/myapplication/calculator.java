package com.compassplus.myapplication;

public class calculator {
    double arg1 = 0.0d;
    double arg2 = 0.0d;

    char operation = ' ';

    void setOperands(double first, double second) {
        arg1 = first;
        arg2 = second;
    }

    void setAddition() {
        operation = '+';
    }

    void setSubtraction() {
        operation = '-';
    }

    void setMultiplication() {
        operation = '*';
    }

    void setDivision() {
        operation = '/';
    }

    void clearOperation() {
        operation = ' ';
    }

    boolean isOperationDefined() {
        return operation != ' ';
    }

    boolean isOperationAllowed() {
        return (!(Math.abs(arg2) < 0.000001 && operation == '/'));
    }

    double doOperation() throws Exception {
        double result = 0.0d;
        if(!isOperationDefined()){
            throw new ArithmeticException("Operation is not defined");
        }
        if(!isOperationAllowed()){
            throw new IllegalArgumentException("Argument can't be zero");
        }

            switch (operation) {
                case '+':
                    result = arg1 + arg2;
                    break;
                case '-':
                    result = arg1 - arg2;
                    break;
                case '*':
                    result = arg1 * arg2;
                    break;
                case '/':
                    result = arg1 / arg2;
            }
        return result;
    }
}
